/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Chain, HttpClient, Interceptor, Request, RequestBody, Response, TimeUnit } from '@ohos/httpclient';
import { CryptoJS } from '@ohos/crypto-js'
import Log from '../model/log'
import { BusinessError } from '@ohos/httpclient/src/main/ets/http';

const secretKey: string = 'abcd1234'

@Entry
@Component
struct Auxiliary {
  @State message: string = 'Hello World'
  @State status: string = "";
  @State content: string = "";
  echoServer: string = "https://postman-echo.com/get";
  client: HttpClient = new HttpClient
    .Builder()
    .setConnectTimeout(10, TimeUnit.SECONDS)
    .setReadTimeout(10, TimeUnit.SECONDS)
    .build();

  build() {
    Column() {
      Flex({
        direction: FlexDirection.Column
      }) {
        Navigator({
          target: "",
          type: NavigationType.Back
        }) {
          Text('BACK')
            .fontSize(12)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Flex({
        direction: FlexDirection.Column
      }) {
        Button('AES')
          .width('80%')
          .height('100%')
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .margin(10)
          .onClick((event: ClickEvent) => {
            let client: HttpClient = new HttpClient.Builder()
              .addInterceptor(new CustomInterceptor())
              .setConnectTimeout(10000).build();
            let request: Request = new Request.Builder()
              .post()
              .body(RequestBody.create("test123"))
              .url(this.echoServer)
              .build();

            client.newCall(request).execute().then((result: Response) => {
              if (result) {
                this.status = result.responseCode.toString();
              }
              if (result.result) {
                this.content = result.result;
              } else {
                this.content = JSON.stringify(result);
              }
            }).catch((error: BusinessError<string>) => {
              this.status = error.code.toString();
              if (error.data != undefined) {
                this.content = error.data;
              }
            });
          })
      }
      .height('10%')
      .width('100%')
      .padding(10)


      Text(this.status)
        .fontSize(20)
        .fontWeight(FontWeight.Bold);
      Text(this.content)
        .fontSize(20)

    }.width('100%').margin({
      top: 5,
      bottom: 100
    }).height('80%')
  }
}

class CustomInterceptor implements Interceptor {
  intercept(chain: Chain): Promise<Response> {
    return new Promise<Response>((resolve, reject)=> {
      let request: Request = chain.requestI();
      Log.showInfo("request = " + request)
      Log.showInfo("inside AES interceptor request" + JSON.stringify(request.body.content))
      let encrypted: string = CryptoJS.AES.encrypt(request.body.content, CryptoJS.enc.Utf8.parse(secretKey), {
        iv: CryptoJS.enc.Utf8.parse('0000000000'),
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
        format: CryptoJS.format.Hex
      }).toString()
      request.body.content = encrypted;

      let response: Promise<Response> = chain.proceedI(request)
      Log.showInfo("response = " + response)
      response.then((data: Response) => {
        resolve(data)
        Log.showInfo("inside AES interceptor response")
        let decrypted: string = CryptoJS.AES.decrypt(data.result, CryptoJS.enc.Utf8.parse(secretKey), {
          iv: CryptoJS.enc.Utf8.parse('0000000000'),
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7,
          format: CryptoJS.format.Hex
        }).toString()
        console.log("AES decrypt = " + JSON.stringify(decrypted));
        data.result = decrypted;
      }).catch((err: BusinessError) => {
        reject(err)
      })
    })
  }
}