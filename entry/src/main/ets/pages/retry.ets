/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { HttpClient, Mime, Request, RequestBody, Response } from '@ohos/httpclient'
import Log from '../model/log'
import router from '@ohos.router';
import hilog from '@ohos.hilog';
import HttpCall from '@ohos/httpclient/src/main/ets/HttpCall';
import { BusinessError } from '@ohos.base';

@Entry
@Component
struct Retry {
  @State status: string = '000';
  @State content: string = 'init';
  echoServer: string = "http://www.yourserverfortest.com";
  client: HttpClient = new HttpClient.Builder().setConnectTimeout(10000).build();

  build() {

    Scroll() {
      Column() {
        Flex({ direction: FlexDirection.Column }) {
          Navigator({ target: "", type: NavigationType.Back }) {
            Text('BACK')
              .fontSize(12)
              .border({ width: 1 })
              .padding(10)
              .fontColor(0x000000)
              .borderColor(0x317aff)
          }
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Text(this.status)
          .fontSize(20)
          .fontWeight(FontWeight.Bold);
        Text(this.content)
          .fontSize(20)

        //请求前取消请求
        Flex({ direction: FlexDirection.Column }) {
          Button('CanceledBeforeExecute')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              let request: Request = new Request.Builder()
                .get("http://hshapp.ncn.com.cn/wisdom3/config/config.do")
                .addHeader("Content-Type", "application/json")
                .params("testKey1", "testValue1")
                .params("testKey2", "testValue2")
                .build();
              let call: HttpCall = this.client.newCall(request)
              call.cancel()
              call.execute().then((data: Response) => {
                Log.showInfo("RetryAndFollowUpInterceptor result   " + JSON.stringify(data));
                if (data) {
                  this.status = data.responseCode.toString();
                }
                if (data.result) {
                  this.content = data.result;
                } else {
                  this.content = JSON.stringify(data);
                }
              }).catch((error: BusinessError) => {
                this.status = error.code.toString();
                if (error.message != undefined) {
                  this.content = error.message;
                }
              });
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        //连接服务器超时
        Flex({ direction: FlexDirection.Column }) {
          Button('ConnectionServerTimeout')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              let request: Request = new Request.Builder()
                .get("http://1.15.87.213:9527/timeoutApi/timeoutTest")
                .addHeader("Content-Type", "application/json")
                .params("testKey1", "testValue1")
                .params("testKey2", "testValue2")
                .build();
              let call: HttpCall = this.client.newCall(request)
              call.execute().then((data: Response) => {
                Log.showInfo("RetryAndFollowUpInterceptor result   " + JSON.stringify(data));
                if (data) {
                  this.status = data.responseCode.toString();
                }
                if (data.result) {
                  this.content = data.result;
                } else {
                  this.content = JSON.stringify(data);
                }
              }).catch((error: BusinessError) => {
                this.status = error.code.toString();
                if (error.message != undefined) {
                  this.content = error.message;
                }
              });
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        //408 第一次请求408，重试请求，第二次仍为408，不再请求返回结果
        Flex({ direction: FlexDirection.Column }) {
          Button('getClientRequestTimeout - 408')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              let request: Request = new Request.Builder()
                .get("http://httpbin.org/delay/2")
                .addHeader("Content-Type", "application/json")
                .addHeader("Connection", "Close")
                .debugCode(408)
                .build();
              this.client.newCall(request)
                .execute()
                .then((result: Response) => {
                  if (result) {
                    this.status = result.responseCode.toString();
                  }
                  if (result.result) {
                    this.content = result.result;
                  } else {
                    this.content = JSON.stringify(result);
                  }
                })
                .catch((error: BusinessError) => {
                  this.status = error.code.toString();
                  if (error.message != undefined) {
                    this.content = error.message;
                  }
                  hilog.info(0x0001, "onError -> Error", JSON.stringify(error));
                });
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        //503 第一次请求503，重试请求，第二次仍为503，不再请求返回结果
        Flex({ direction: FlexDirection.Column }) {
          Button('retryOnUnavailableRetryAfter - 503')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              let request: Request = new Request.Builder()
                .get("https://mock.apifox.cn/m1/1773132-0-default/pet/1")
                .addHeader("Content-Type", "application/json")
                .addHeader("Connection", "Close")
                .debugCode(503)
                .build();
              this.client.newCall(request)
                .execute()
                .then((result: Response) => {
                  if (result) {
                    this.status = result.responseCode.toString();
                  }
                  if (result.result) {
                    this.content = result.result;
                  } else {
                    this.content = JSON.stringify(result);
                  }
                })
                .catch((error: BusinessError) => {
                  this.status = error.code.toString();
                  if (error.message != undefined) {
                    this.content = error.message;
                  }
                  hilog.info(0x0001, "onError -> Error", JSON.stringify(error));
                });
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        //401
        Flex({ direction: FlexDirection.Column }) {
          Button('authenticator - 401')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/authenticator'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)
        //401
        Flex({ direction: FlexDirection.Column }) {
          Button('authenticator_custom- 401')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              router.pushUrl({
                url: 'pages/authenticator_custom'
              })
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('setFollowRedirectsFalse - 302')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              let request: Request = new Request.Builder()
                .url("https://mock.apifox.cn/m2/1773132-0-default/44759484")
                .post(RequestBody.create(new Mime.Builder().contentType('application/json', 'charset', 'utf8')
                  .build()
                  .getMime()))
                .followRedirects(false)
                .build();
              this.client.newCall(request)
                .execute()
                .then((result: Response) => {
                  if (result) {
                    this.status = result.responseCode.toString();
                  }
                  if (result.result) {
                    this.content = result.result;
                  } else {
                    this.content = JSON.stringify(result);
                  }
                  Log.showInfo("onComplete -> Status : " + this.status);
                  Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
                })
                .catch((error: BusinessError) => {
                  this.status = error.code.toString();
                  if (error.message != undefined) {
                    this.content = error.message;
                  }
                  hilog.info(0x0001, "onError -> Error", this.content);
                });
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('redirectRequest - 307')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              let request: Request = new Request.Builder()
                .url("https://mock.apifox.cn/m1/1773132-0-default/redirectRequest?apifoxResponseId=111715800")
                .post(RequestBody.create(new Mime.Builder().contentType('application/json', 'charset', 'utf8')
                  .build()
                  .getMime()))
                .debugUrl('https://mock.apifox.cn/m1/1773132-0-default/redirectRequest')
                .build();
              this.client.newCall(request)
                .execute()
                .then((result: Response) => {
                  if (result) {
                    this.status = result.responseCode.toString();
                  }
                  if (result.result) {
                    this.content = result.result;
                  } else {
                    this.content = JSON.stringify(result);
                  }
                  Log.showInfo("onComplete -> Status : " + this.status);
                  Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
                })
                .catch((error: BusinessError) => {
                  this.status = error.code.toString();
                  if (error.message != undefined) {
                    this.content = error.message;
                  }
                  hilog.info(0x0001, "onError -> Error", this.content);
                });
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Flex({ direction: FlexDirection.Column }) {
          Button('requestException')
            .width('80%')
            .height('100%')
            .fontSize(18)
            .fontColor(0xCCCCCC)
            .align(Alignment.Center)
            .margin(10)
            .onClick((event: ClickEvent) => {
              let request: Request = new Request.Builder()
                .url(this.echoServer)
                .post(RequestBody.create("123"))
                .addHeader("Content-Type", "application/json")
                .build();
              this.client.newCall(request)
                .execute()
                .then((result: Response) => {
                  if (result) {
                    this.status = result.responseCode.toString();
                  }
                  if (result.result) {
                    this.content = result.result;
                  } else {
                    this.content = JSON.stringify(result);
                  }
                  Log.showInfo("onComplete -> Status : " + this.status);
                  Log.showInfo("onComplete -> Content : " + JSON.stringify(this.content));
                })
                .catch((error: BusinessError) => {
                  this.status = error.code.toString();
                  if (error.message != undefined) {
                    this.content = error.message;
                  }
                  hilog.info(0x0001, "onError -> Error", this.content);
                });
            })
        }
        .height('10%')
        .width('100%')
        .padding(10)

        Blank().height(150)
      }
    }.scrollable(ScrollDirection.Vertical)
    .width('100%')
    .height('100%')
    .margin({ top: 5, bottom: 100 })

  }
}
